### ChatCAT - A Realtime Multiroom Chat Application ###

* Built a multiroom chat server using Express.js and Node.js. Created frontend pages with HTML, CSS, Javascript and jQuery. Stored user profiles and managed Sessions using MongoLab.
* Created Facebook and Twitter Social Auth using Passport.js. Developed chatroom's features with Socket.io.
* Deployed the App on Heroku. Hosted images with Cloudinary CDN. Scaled up data storage using Redis. 

### Demo URL ###
* chatcat-app.herokuapp.com
* Support Chrome and Firefox

### Run Configuration ###

* Please modify these configs: MongolabURI, Redis, Cloudinary, Heroku, etc
* change to development mode
* In root folder, type command "npm install"
* navigate to "/server folder", type command "node app.js"